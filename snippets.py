#print data

#print(q.url)
#print q
#print q.headers
#print "blabla"
#print q.content
#print json.dumps(data)
#unit['city'] = result[0][2]
#unit['descriptions']['description_note'] = "es hat funktioniert"
#json.dumps() gives str representation of dict object
#unit = json.dumps(unit, indent=4, sort_keys=True)
#print type(unit)
#unit = unit.decode("unicode-escape")
#file_object  = open('kastebeispiel.json', 'w')
#file_object.write(unit.encode('utf-8'))
#file_object.close() 
#print unit


#### MINIMALANGABEN
data = {}
data['property'] = {}
data['property']['marketing_type'] = 'BUY'  ###
data['property']['object_type'] = 'LIVING'  ###
data['property']['rs_type'] = 'HOUSE'       ###
#data['property']['exposee_id'] = ''
data['property']['country'] = 'Deutschland' ###
data['property']['city'] = 'Hannover'       ###
data['property']['zip_code'] = '30167'      ###
data['property']['street'] = 'Im Othfelde'  ###
data['property']['house_number'] = '65'     ###
#data['property']['address'] = 'das ist eine Probeadresse' ###
#data['property']['sublocality_level_1'] = ''
#data['property']['administrative_area_level_1'] = ''
#data['property']['administrative_area_level_2'] = None
data['property']['lat'] = 52.222            ###
data['property']['lng'] = 13.4545           ###
data['property']['price'] = 20000           ###
#data['property']['number_of_bed_rooms'] = 4
#data['property']['number_of_bath_rooms'] = 4
data['property']['number_of_rooms'] = 8
data['property']['living_space'] = 120.0
data['property']['plot_area'] = 123
data['property']['building_type'] = 'NO_INFORMATION'