import json


#macht ein dict aus der beispiel-datei
with open('kastebeispiel.json') as json_data:
    unit = json.load(json_data)
print type(unit)
#macht einen str aus dem dict
unit = json.dumps(unit, indent=4, sort_keys=True)
print type(unit)
#macht einen unicode-str aus dem str
unit = unit.decode("unicode-escape")
print type(unit)