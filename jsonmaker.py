import MySQLdb
import json
import requests
import re

#### TODO default-broker anlegen und hinzufuegen
#### TODO Meerblick in import_log eintragen
#### TODO ausprobieren ob import_log propstack id fehler anzeigt wenn id nicht gefunden


def writetopropstack(Id):
    db = MySQLdb.connect("localhost","root","","kastebase")
    cursor = db.cursor()
    db.autocommit(1)
    #Id = 629
    #Id = 1109270
    #Id = 1575721
    #Id=1396670
    #Id = 970044
    #Id = 1038998

    prop = {}
    prop ['property'] = {}
    ### Lege mir einen Eintrag im import_log mit onoffice_externe_id und onoffice_id an
    cursor.execute("SELECT Id, objektnr_extern FROM kastebase.ObjTech WHERE Id='"+str(Id)+"';")
    result = cursor.fetchall()
    onoffice_id = str(result [0][0])
    externe_onoffice_id = str(result [0][1])
    prop['property']['old_crm_id'] = result[0][0]

    cursor.execute("INSERT INTO import_log (onoffice_id, externe_onoffice_id) VALUES ('"+str(onoffice_id)+"','"+str(externe_onoffice_id)+"');")
    ### Hole mir Vermarktungsart, Nutzungsart und Gebaeudetyp
    ### Hier entscheidet sich, welche Felder gesetzt werden
    cursor.execute("SELECT Id, vermarktungsart, nutzungsart, objektart, objekttyp FROM kastebase.ObjKategorie WHERE Id='"+str(Id)+"';")
    result = cursor.fetchall()
        
    ### Setze vermarktungsart/marketing_type
    if result[0][1] == 'kauf':
        prop['property']['marketing_type'] = 'BUY'
    elif result[0][1] == 'miete':
        prop['property']['marketing_type'] = 'RENT'
    else:
        cursor.execute("UPDATE import_log SET fehler = CONCAT(IFNULL(fehler,''), ' vermarktungsart') WHERE onoffice_id = '"+str(Id)+"';")
        
    ### Setze nutzungsart/object_type
    if result[0][2] == 'wohnen':
        prop['property']['object_type'] = 'LIVING'
    elif result[0][2] == 'gewerbe':
        prop['property']['object_type'] = 'COMMERCIAL'
    elif result[0][2] == 'anlage':
        prop['property']['object_type'] = 'INVESTMENT'
    ### Nicht gesetzte Nutzungsart abfangen zu default INVESTMENT
    elif result[0][2] == '':
        prop['property']['object_type'] = 'INVESTMENT'
    elif result[0][2] == 'waz':
        prop['property']['object_type'] = 'LIVING'
        prop['property']['rs_type'] = 'SHORT_TERM_ACCOMODATION'
    else:
        cursor.execute("UPDATE import_log SET fehler = CONCAT(IFNULL(fehler,''), ' nutzungsart') WHERE onoffice_id = '"+str(Id)+"';")

    ### Setze objektart/rs_type
    if result[0][3] == 'haus':
        prop['property']['rs_type'] = 'HOUSE'
    elif result[0][3] == 'wohnung':
        prop['property']['rs_type'] = 'APARTMENT'
    elif result[0][3] == 'grundstueck':
        prop['property']['rs_type'] = 'TRADE_SITE'
    elif result[0][3] == 'einzelhandel':
        prop['property']['rs_type'] = 'STORE'
    elif result[0][3] == 'buero_praxen':
        prop['property']['rs_type'] = 'OFFICE'
    elif result[0][3] == 'hallen_lager_prod':
        prop['property']['rs_type'] = 'INDUSTRY'
    elif result[0][3] == 'gastgewerbe':
        prop['property']['rs_type'] = 'GASTRONOMY'
    elif result[0][3] == 'freizeitimmbilien_gewerblich':
        prop['property']['rs_type'] = 'SPECIAL_PURPOSE'
    elif result[0][3] == 'land_und_forstwirtschaft':
        prop['property']['rs_type'] = 'SPECIAL_PURPOSE'
    elif result[0][3] == 'zimmer':
        prop['property']['rs_type'] = 'SHORT_TERM_ACCOMODATION'
    elif result[0][3] == 'sonstige':
        prop['property']['rs_type'] = 'SPECIAL_PURPOSE'
    elif result[0][3] == '':
        if prop['property']['object_type'] == 'LIVING':
            prop['property']['rs_type'] = 'HOUSE'
        if prop['property']['object_type'] == 'COMMERCIAL':
            prop['property']['rs_type'] = 'INDUSTRY'
        if prop['property']['object_type'] == 'INVESTMENT':
            prop['property']['rs_type'] = 'INVESTMENT'
    else:
        cursor.execute("UPDATE import_log SET fehler = CONCAT(IFNULL(fehler,''), ' objektart/rs_type') WHERE onoffice_id = '"+str(Id)+"';")

    ###
    ### Hier werden die objekttypen verarbeitet
    ###

    ### Setzte objekttyp/apartment_type
    if prop['property']['rs_type'] == 'APARTMENT':
        if result[0][4] == 'dachgeschoss':
            prop['property']['apartment_type'] = 'ROOF_STOREY'
        elif result[0][4] == 'etage':
            prop['property']['apartment_type'] = 'APARTMENT'
        elif result[0][4] == 'erdgeschoss':
            prop['property']['apartment_type'] = 'GROUND_FLOOR'
        elif result[0][4] == 'loft-studio-atelier':
            prop['property']['apartment_type'] = 'LOFT'
        elif result[0][4] == 'maisonette':
            prop['property']['apartment_type'] = 'MAISONETTE'
        elif result[0][4] == 'souterrain':
            prop['property']['apartment_type'] = 'HALF_BASEMENT'
        elif result[0][4] == 'hochparterre':
            prop['property']['apartment_type'] = 'RAISED_GROUND_FLOOR'
        elif result[0][4] == 'penthouse':
            prop['property']['apartment_type'] = 'PENTHOUSE'
        elif result[0][4] == 'eigentumswohnung':
            prop['property']['apartment_type'] = 'APARTMENT'
        elif result[0][4] == 'terrassen':
            prop['property']['apartment_type'] = 'TERRACED_FLAT'
        elif result[0][4] == 'sonstige':
            prop['property']['apartment_type'] = 'OTHER'
        elif result[0][4] == '':
            prop['property']['apartment_type'] = 'NO_INFORMATION'
        else:
            cursor.execute("UPDATE import_log SET fehler = CONCAT(IFNULL(fehler,''), ' objekttyp/apartment_type') WHERE onoffice_id = '"+str(Id)+"';")

    ### Setze objekttyp/building_type
    if prop['property']['rs_type'] == 'HOUSE':
        if result[0][4] == 'villa':
            prop['property']['building_type'] = 'VILLA'
        elif result[0][4] == 'einfamilienhaus':
            prop['property']['building_type'] = 'SINGLE_FAMILY_HOUSE'
        elif result[0][4] in ('zweifamilienhaus','mehrfamilienhaus'):
            prop['property']['building_type'] = 'MULTI_FAMILY_HOUSE'
        elif result[0][4] in ('reihenend','reiheneck'):
            prop['property']['building_type'] = 'END_TERRACE_HOUSE'
        elif result[0][4] in ('reihenhaus','reihenmittel'):
            prop['property']['building_type'] = 'MID_TERRACE_HOUSE'
        elif result[0][4] == 'doppelhaushaelfte':
            prop['property']['building_type'] = 'SEMIDETACHED_HOUSE'
        elif result[0][4] in ('bauernhaus', 'resthof', 'landhaus'):
            prop['property']['building_type'] = 'FARMHOUSE'
        elif result[0][4] in ('bungalow', 'ferienhaus','strandhaus'):
            prop['property']['building_type'] = 'BUNGALOW'
        elif result[0][4] in ('stadthaus', 'wohn_und_geschaeftshaus', '', 'chalet'):
            prop['property']['building_type'] = 'OTHER'
        else:
            cursor.execute("UPDATE import_log SET fehler = CONCAT(IFNULL(fehler,''), ' objekttyp/building_type') WHERE onoffice_id = '"+str(Id)+"';")

    ### Setze objekttyp/building_type
    if prop['property']['rs_type'] == 'OFFICE':
        if result[0][4] == 'bueroflaeche':
            prop['property']['office_type'] = 'OFFICE'
        if result[0][4] == 'loft':
            prop['property']['office_type'] = 'LOFT'
        if result[0][4] == 'buerohaus':
            prop['property']['office_type'] = 'OFFICE_BUILDING'
        if result[0][4] == 'bueroetage':
            prop['property']['office_type'] = 'OFFICE_FLOOR'
        if result[0][4] == 'praxis':
            prop['property']['office_type'] = 'SURGERY'
        if result[0][4] == 'buerozentrum':
            prop['property']['office_type'] = 'OFFICE_CENTRE'
        if result[0][4] == 'buero_und_lager':
            prop['property']['office_type'] = 'OFFICE_STORAGE_BUILDING'
        if result[0][4] == 'praxishaus':
            prop['property']['office_type'] = 'SURGERY_BUILDING'
        if result[0][4] == 'praxisflaeche':
            prop['property']['office_type'] = 'SURGERY_FLOOR'
        if result[0][4] == '':
            prop['property']['office_type'] = 'OFFICE_AND_COMMERCIAL_BUILDING'
        else:
            cursor.execute("UPDATE import_log SET fehler = CONCAT(IFNULL(fehler,''), ' objekttyp/office_type') WHERE onoffice_id = '"+str(Id)+"';")

    ### Setze objekttyp/gastronomy_type_options
    if prop['property']['rs_type'] == 'GASTRONOMY':
        if result[0][4] == 'gastronomie':
            prop['property']['gastronomy_type'] = 'RESTAURANT'
        if result[0][4] == 'hotels':
            prop['property']['gastronomy_type'] = 'HOTEL'
        if result[0][4] == 'restaurant':
            prop['property']['gastronomy_type'] = 'RESTAURANT'
        if result[0][4] == 'ladenlokal':
            prop['property']['gastronomy_type'] = 'TAVERN'
        if result[0][4] == '':
            prop['property']['gastronomy_type'] = 'RESTAURANT'
        else:
            cursor.execute("UPDATE import_log SET fehler = CONCAT(IFNULL(fehler,''), ' objekttyp/gastronomy_type') WHERE onoffice_id = '"+str(Id)+"';")

    ### Setze objekttyp/industry_type_options
    if prop['property']['rs_type'] == 'INDUSTRY':
        if result[0][4] == 'lager':
            prop['property']['industry_type'] = 'STORAGE_AREA'
        if result[0][4] == 'lagerflaeche':
            prop['property']['industry_type'] = 'STORAGE_AREA'
        if result[0][4] == 'halle':
            prop['property']['industry_type'] = 'HALL'
        if result[0][4] == 'hochregallager':
            prop['property']['industry_type'] = 'HIGH_LACK_STORAGE'
        if result[0][4] == 'industriehalle':
            prop['property']['industry_type'] = 'INDUSTRY_HALL'
        if result[0][4] == 'werkstatt':
            prop['property']['industry_type'] = 'REPAIR_SHOP'
        if result[0][4] == '' or result[0][4] == None:
            prop['property']['industry_type'] = 'INDUSTRY_HALL'
        else:
            cursor.execute("UPDATE import_log SET fehler = CONCAT(IFNULL(fehler,''), ' objekttyp/industry_type') WHERE onoffice_id = '"+str(Id)+"';")

    ### Setze objekttyp/store_type_options
    if prop['property']['rs_type'] == 'STORE':
        if result[0][4] == 'ladenlokal':
            prop['property']['store_type'] = 'STORE'
        if result[0][4] == '':
            prop['property']['store_type'] = 'NO_INFORMATION'
        if result[0][4] == 'ausstellungsflaeche':
            prop['property']['store_type'] = 'SHOWROOM_SPACE'
        else:
            cursor.execute("UPDATE import_log SET fehler = CONCAT(IFNULL(fehler,''), ' objekttyp/store_type') WHERE onoffice_id = '"+str(Id)+"';")

    ### Setze objekttyp/special_purpose_type_options
    ### hat keine entsprechung in onoffice

    ### Setze objekttyp/investment_type_options
    if prop['property']['rs_type'] == 'STORE':
        if result[0][4] in ('mehrfamilienhaus', 'zweifamilienhaus'):
            prop['property']['investment_type'] = 'MULTI_FAMILY_HOUSE'
        if result[0][4] == '':
            prop['property']['investment_type'] = 'OTHER'
        if result[0][4] == 'buerohaus':
            prop['property']['investment_type'] = 'OFFICE_BUILDING'
        if result[0][4] in ('penthouse', 'etage' ,'maisonette' ,'eigentumswohnung' ,'dachgeschoss' ,'erdgeschoss', 'hochparterre'):
            prop['property']['investment_type'] = 'FREEHOLD_FLAT'
        if result[0][4] == 'ladenlokal':
            prop['property']['investment_type'] = 'SHOP_SALES_FLOOR'
        if result[0][4] == 'reihenhaus':
            prop['property']['investment_type'] = 'SINGLE_FAMILY_HOUSE'
        if result[0][4] == 'einfamilienhaus':
            prop['property']['investment_type'] = 'SINGLE_FAMILY_HOUSE'
        if result[0][4] == 'wohn_und_geschaeftshaus':
            prop['property']['investment_type'] = 'LIVING_BUSINESS_HOUSE'
        if result[0][4] == 'praxishaus':
            prop['property']['investment_type'] = 'SURGERY_BUILDING'
        if result[0][4] == 'bueroflaeche':
            prop['property']['investment_type'] = 'OFFICE_BUILDING'
        if result[0][4] == 'parkhaus':
            prop['property']['investment_type'] = 'OTHER'
        if result[0][4] == 'reihenend':
            prop['property']['investment_type'] = 'SINGLE_FAMILY_HOUSE'
        if result[0][4] == '':
            prop['property']['investment_type'] = 'OTHER'
        else:
            cursor.execute("UPDATE import_log SET fehler = CONCAT(IFNULL(fehler,''), ' objekttyp/investment_type') WHERE onoffice_id = '"+str(Id)+"';")


    ### Hole mir die Geodaten und Etage
    cursor.execute("SELECT Id, strasse, hausnummer, plz, ort, land, regionaler_zusatz, breitengrad, laengengrad, etage FROM kastebase.ObjGeo WHERE Id='"+str(Id)+"';")
    result = cursor.fetchall()

    ### Setze geodaten
    ### Strasse setzen:
    if result[0][1] == '':
        prop['property']['street'] = 'no_value'
        cursor.execute("UPDATE import_log SET fehler = CONCAT(IFNULL(fehler,''), ' keine strasse') WHERE onoffice_id = '"+str(Id)+"';")
    else:
        prop['property']['street'] = result[0][1].decode('iso-8859-1').encode('utf8')
    
    ### Hausnummer setzen:
    if result[0][2] == '':
        prop['property']['house_number'] = '404'
        cursor.execute("UPDATE import_log SET fehler = CONCAT(IFNULL(fehler,''), ' keine_nummer') WHERE onoffice_id = '"+str(Id)+"';")
    else:
        prop['property']['house_number'] = result[0][2].decode('iso-8859-1').encode('utf8')
    
    ### PLZ setzen:
    if result[0][3] == '':
        prop['property']['zip_code'] = '404'
        cursor.execute("UPDATE import_log SET fehler = CONCAT(IFNULL(fehler,''), ' keine_plz') WHERE onoffice_id = '"+str(Id)+"';")
    else:
        prop['property']['zip_code'] = result[0][3].decode('iso-8859-1').encode('utf8')

    ### Stadt setzen:
    if result[0][4] == '':
        prop['property']['city'] = 'no_value'
        cursor.execute("UPDATE import_log SET fehler = CONCAT(IFNULL(fehler,''), ' keine_stadt') WHERE onoffice_id = '"+str(Id)+"';")
    else:
        prop['property']['city'] = result[0][4].decode('iso-8859-1').encode('utf8') 

    ### Land setzen:      
    if result[0][5] == '':
        prop['property']['country'] = 'no_country'
        cursor.execute("UPDATE import_log SET fehler = CONCAT(IFNULL(fehler,''), ' keine_stadt') WHERE onoffice_id = '"+str(Id)+"';")
    else:
       prop['property']['country'] = result[0][5].decode('iso-8859-1').encode('utf8')      
    prop['property']['lat'] = result[0][7]     
    prop['property']['lng'] = result[0][8]
    prop['property']['floor'] = result [0][9]

    ### Hole mir Preise
    cursor.execute("SELECT Id, kaufpreis, kaltmiete, nebenkosten, aussen_courtage FROM kastebase.ObjPreise WHERE Id='"+str(Id)+"';")
    result = cursor.fetchall()

    ### Setze Preise 
    if prop['property']['marketing_type'] == 'BUY':
        prop['property']['price'] = float(result[0][1])
        prop['property']['has_courtage'] = True
        if result[0][4] == '' or result[0][4] == None:
            prop['property']['courtage'] = "auf Anfrage"
        else:
            prop['property']['courtage'] = result[0][4].decode('iso-8859-1').encode('utf8')
    if prop['property']['marketing_type'] == 'RENT':
        prop['property']['base_rent'] = float(result[0][2])
        prop['property']['service_charge'] = float(result[0][3])
        prop['property']['has_courtage'] = False
        
    ### Hole mir die Texte und Titel
    cursor.execute("SELECT Id, objekttitel, objektbeschreibung, ausstatt_beschr, lage, sonstige_angaben FROM kastebase.ObjFreitexte WHERE Id='"+str(Id)+"';")
    result = cursor.fetchall()
    prop['property']['title'] = result[0][1].decode('iso-8859-1').encode('utf8')
    prop['property']['description_note'] = result[0][2].decode('iso-8859-1').encode('utf8')
    prop['property']['furnishing_note'] = result[0][3].decode('iso-8859-1').encode('utf8')
    prop['property']['location_note'] = result[0][4].decode('iso-8859-1').encode('utf8')
    prop['property']['other_note'] = result[0][5].decode('iso-8859-1').encode('utf8')


    ### Hole mir Flaechen, Raueme etc.
    cursor.execute("SELECT Id, wohnflaeche, grundstuecksflaeche, anzahl_zimmer, anzahl_schlafzimmer, anzahl_badezimmer, gesamtflaeche, nutzflaeche FROM kastebase.ObjFlaeche WHERE Id='"+str(Id)+"';")
    result = cursor.fetchall()
    ### Setze Flaechen/Raueme
    prop['property']['living_space'] = result[0][1]
    prop['property']['plot_area'] = result[0][2]
    prop['property']['number_of_rooms'] = result[0][3]
    if prop['property']['object_type'] == 'LIVING':
        prop['property']['number_of_bed_rooms'] = result[0][4]
        prop['property']['number_of_bath_rooms'] = result[0][5]
    if prop['property']['object_type'] in ('COMMERCIAL', 'INVESTMENT'):
        prop['property']['total_floor_space'] = result[0][6]
        prop['property']['net_floor_space'] = result[0][7]

    ### Hole mir Objektzustand
    cursor.execute("SELECT Id, baujahr, zustand FROM kastebase.ObjZustand WHERE Id='"+str(Id)+"';")
    result = cursor.fetchall()
    # Setze Baujahr    
    if result[0][1] != "":
        prop['property']['construction_year'] = result[0][1]
    # Setze Zustand
    if result[0][2] != "":
        if result[0][2] == 'modernisiert':
            prop['property']['condition'] = 'MODERNIZED'
        if result[0][2] == 'vollstaendig_renoviert':
            prop['property']['condition'] = 'FULLY_RENOVATED'
        if result[0][2] == '':
            prop['property']['condition'] = 'NO_INFORMATION'
        if result[0][2] == 'renovierungsbeduerftig':
            prop['property']['condition'] = 'NEED_OF_RENOVATION'
        if result[0][2] == 'neuwertig':
            prop['property']['condition'] = 'MINT_CONDITION'
        if result[0][2] == 'gepflegt':
            prop['property']['condition'] = 'WELL_KEPT'
        if result[0][2] == 'nach_vereinbarung':
            prop['property']['condition'] = 'NEGOTIABLE'
        if result[0][2] == 'erstbezug':
            prop['property']['condition'] = 'FIRST_TIME_USE'
        if result[0][2] == 'erstbezug_nach_sanierung':
            prop['property']['condition'] = 'FIRST_TIME_USE_AFTER_REFURBISHMENT'
        if result[0][2] == 'rohbau':
            prop['property']['condition'] = 'NO_INFORMATION'
        if result[0][2] == 'saniert':
            prop['property']['condition'] = 'REFURBISHED'
        if result[0][2] == 'projektiert':
            prop['property']['condition'] = 'NO_INFORMATION'
        if result[0][2] == 'baufaellig':
            prop['property']['condition'] = 'NEED_OF_RENOVATION'
        if result[0][2] == 'abrissobjekt':
            prop['property']['condition'] = 'RIPE_FOR_DEMOLITION'
        else:
            prop['property']['condition'] = ''
            cursor.execute("UPDATE import_log SET fehler = CONCAT(IFNULL(fehler,''), ' kein_zustand') WHERE onoffice_id = '"+str(Id)+"';")

    ### Hole mir Energie 
    cursor.execute("SELECT Id, energieausweistyp, energieausweis_gueltig_bis, energieverbrauchskennwert, energyClass, endenergiebedarf, energietraeger FROM kastebase.ObjZustand WHERE Id='"+str(Id)+"';")
    result = cursor.fetchall()
    ### Setze Energieausweistyp
    if result[0][1] == '':
        prop['property']['energy_certificate_availability'] = 'NO_INFORMATION'
        prop['property']['building_energy_rating_type'] = 'NO_INFORMATION'
    if result[0][1] in ('Energieverbrauchskennwert','Verbrauchsausweis Gewerbe'):
        prop['property']['building_energy_rating_type'] = 'ENERGY_CONSUMPTION'
        prop['property']['energy_certificate_availability'] = 'AVAILABLE'
    if result[0][1] in ('Endenergiebedarf','Bedarfsausweis Gewerbe'):
        prop['property']['building_energy_rating_type'] = 'ENERGY_REQUIRED'
        prop['property']['energy_certificate_availability'] = 'AVAILABLE'
    ### Setze Energieausweiseablaufdatum
    ### Hat keine Entsprechung in Propstack

    ### Setze Energietraeger
    if result[0][6] == '':
        prop['property']['firing_types'] = 'NO_INFORMATION'
    if result[0][6] == None:
        prop['property']['firing_types'] = 'NO_INFORMATION'
    if result[0][6] == 'gas':
        prop['property']['firing_types'] = 'GAS'
    if result[0][6] == 'luftwp':
        prop['property']['heating_type'] = 'HEAT_PUMP'
    if result[0][6] == 'fernwaerme':
        prop['property']['firing_types'] = 'DISTRICT_HEATING'
    if result[0][6] == 'oel':
        prop['property']['firing_types'] = 'OIL'
    if result[0][6] == 'strom':
        prop['property']['firing_types'] = 'ELECTRICITY'
    if result[0][6] == 'elektro':
        prop['property']['firing_types'] = 'ELECTRICITY'

    ### Heizungsart holen & setzen
    cursor.execute("SELECT Id, heizungsart, meerblick FROM kastebase.ObjAusstattung WHERE Id='"+str(Id)+"';")
    result = cursor.fetchall()
    ### Setzen...
    if result[0][1] == '':
        prop['property']['heating_type'] = 'NO_INFORMATION'
    if result[0][1] == '|zentral|':
        prop['property']['heating_type'] = 'CENTRAL_HEATING'
    if result[0][1] == '|fussboden|':
        prop['property']['heating_type'] = 'FLOOR_HEATING'
    if result[0][1] == '|etage|':
        prop['property']['heating_type'] = 'SELF_CONTAINED_CENTRAL_HEATING'
    if result[0][1] == '|k.A.|':
        prop['property']['heating_type'] = 'NO_INFORMATION'
    if result[0][1] == '|fern|':
        prop['property']['heating_type'] = 'DISTRICT_HEATING'
    if result[0][1] == '|k.A.||zentral|':
        prop['property']['heating_type'] = 'CENTRAL_HEATING'
    if result[0][1] == '|zentral||fussboden|':
        prop['property']['heating_type'] = 'FLOOR_HEATING'
    if result[0][1] == '|ofen|':
        prop['property']['heating_type'] = 'STOVE_HEATING'
    if result[0][1] == '|ofen||zentral|':
        prop['property']['heating_type'] = 'CENTRAL_HEATING'
    if result[0][1] == '|k.A.||etage|':
        prop['property']['heating_type'] = 'SELF_CONTAINED_CENTRAL_HEATING'
    if result[0][1] == '|fussboden||zentral|':
        prop['property']['heating_type'] = 'FLOOR_HEATING'
    if result[0][1] == '|zentral||fern|':
        prop['property']['heating_type'] = 'CENTRAL_HEATING'
    if result[0][1] == '|etage||zentral|':
        prop['property']['heating_type'] = 'CENTRAL_HEATING'
    if result[0][1] == '|ofen||etage|':
        prop['property']['heating_type'] = 'SELF_CONTAINED_CENTRAL_HEATING'
    if result[0][2] == 1:
        cursor.execute("UPDATE import_log SET meerblick = '1' WHERE onoffice_id = '"+str(Id)+"';")


    ### Abschluss: unit erzeugen, propstack_id in import_log setzen, database schliessen
    payload = {'api_key':'ouqILvxz5QD9RROfOeMlxtQ9-RRi06e-KR8X9Mnn'}
    headers = {'content-type': 'application/json'}
    q = requests.post('https://crm.propstack.de/api/v1/units', params=payload , data=json.dumps(prop), headers=headers)
    #print q
    #print q.content
    try:
        found = re.search('id\":(.+?)\}', q.content).group(1)
    except AttributeError:
        # AAA, ZZZ not found in the original string
        found = q.content 
    #{"ok":true,"id":10434}
    cursor.execute("UPDATE import_log SET propstack_id = '"+found+"' WHERE onoffice_id = '"+str(Id)+"';")#TODO id richtig in db setzen
    cursor.execute("UPDATE import_log SET answer = '"+str(q)+"' WHERE onoffice_id = '"+str(Id)+"';")
    #### q.content.decode('utf-8') wieder statt found einfuegen

    db.close()
    return